#!/usr/bin/env python3
import os
from time import sleep

def format_secs(secs):
    mins = secs / 60
    secs %= 60
    return "%02i:%02i" % (mins, secs)

def get_status(status):
    if status == "playing":
        return " "
    elif status == "paused":
        return " "

out = os.popen("cmus-remote -Q")
out = out.read().split('\n')
if len(out) == 1:
    print("")
else:
    ret = []
    status = get_status(out[0].split(' ')[1])
    for i in range(len(out)):
        if out[i].startswith("tag artist"):
            ret.append(out[i][11:])
        elif out[i].startswith("duration") or out[i].startswith("position"):
            ret.append(format_secs(int(out[i][9:])))
        elif out[i].startswith("tag title"):
            ret.append(out[i][10:])
        if len(ret) == 4:
            break
    print(f"{ret[2]} - {ret[3]} {ret[1]}/{ret[0]} {status}")
