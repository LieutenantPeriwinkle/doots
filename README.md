# my doot files

Very little organization, but you should be able to locate everything necessary to replicate my system here. Everything uses the [Victor Mono font](https://rubjo.github.io/victor-mono/) so you'll want to install that.

Here's all the programs necessary to get the full setup

- [kakoune](https://github.com/mawww/kakoune)
- [kak-lsp](https://github.com/kak-lsp/kak-lsp)
- [plug.kak](https://github.com/andreyorst/plug.kak)
- [kitty](https://github.com/kovidgoyal/kitty)
- [xmonad](https://xmonad.org/)
- [xmobar](https://codeberg.org/xmobar/xmobar)
- [starship](https://github.com/starship/starship)
- [cmus](https://github.com/cmus/cmus)
- [rofi](https://github.com/davatorium/rofi)
- [beautifuldiscord](https://github.com/leovoel/BeautifulDiscord)
- [zsh-autosuggest](https://github.com/zsh-users/zsh-autosuggestions)

### Perilized
The colorscheme of pretty much everything is my custom colorscheme, based on Solarized Dark. I call it Perilized.
It has none of the science and careful design behind it like Solarized but I like it more so that's why it's here.

It's fairly trivial to translate from solarized dark to perilized, and in fact that's what most of these themes are.


### Images
desktop background that i never see
![desktop background](images/bg.png?raw=true)

a terminal session
![terminal window](images/term.png?raw=true)

i wake up to another day of neofetch
![neofetch](images/neofetch.png?raw=true)
