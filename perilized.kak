declare-option -hidden str fg0 '8d9499'
declare-option -hidden str fg1 '646566'
declare-option -hidden str fg2 'cccccc'
declare-option -hidden str bg0 '00223d'
declare-option -hidden str bg1 '002a4c'
declare-option -hidden str bg2 '094370'
declare-option -hidden str green '3e991a'
declare-option -hidden str magenta 'e72685'
declare-option -hidden str lblue '2ca6e2'
declare-option -hidden str red 'd01d35'
declare-option -hidden str blue '1764ff'
declare-option -hidden str orange 'e25526'
declare-option -hidden str yellow 'b99916'
declare-option -hidden str violet 'af4de3'

# code

face global value "rgb:%opt{lblue}"
face global type "rgb:%opt{red}"
face global variable "rgb:%opt{blue}"
face global module "rgb:%opt{magenta}"
face global function "rgb:%opt{blue}"
face global string "rgb:%opt{lblue}"
face global keyword "rgb:%opt{green}+i"
face global operator "rgb:%opt{yellow}"
face global parameter "rgb:%opt{blue}"
face global attribute "rgb:%opt{violet}+i"
face global comment "rgb:%opt{fg1}+i"
face global documentation comment
face global meta "rgb:%opt{orange}"
face global builtin default+b

# kakoune UI

face global Default "rgb:%opt{fg0},rgb:%opt{bg0}"
face global PrimarySelection "rgb:%opt{bg1},rgb:%opt{fg0}"
face global SecondarySelection "rgb:%opt{bg1},rgb:%opt{fg1}"
face global PrimaryCursor "rgb:%opt{bg0},rgb:%opt{fg2}"
face global SecondaryCursor "rgb:%opt{bg0},rgb:%opt{fg1}"
face global PrimaryCursorEol "rgb:%opt{bg0},rgb:%opt{fg2}"
face global SecondaryCursorEol "rgb:%opt{bg0},rgb:%opt{fg1}"
face global Whitespace comment
face global BufferPadding comment
face global LineNumbers "rgb:%opt{fg1}"
face global LineNumberCursor "rgb:%opt{fg1},rgb:%opt{bg1}"
face global MenuForeground "rgb:%opt{fg2},rgb:%opt{bg2}"
face global MenuBackground "default,rgb:%opt{bg1}"
face global MenuInfo "rgb:%opt{fg0},rgb:%opt{bg0}"
face global Information "rgb:%opt{bg1},rgb:%opt{fg0}"
face global Error "rgb:%opt{red},rgb:%opt{bg0}"
face global StatusLine "rgb:%opt{fg2},rgb:%opt{bg0}+b"
face global StatusLineMode "rgb:%opt{magenta}"
face global StatusLineInfo "rgb:%opt{lblue}"
face global StatusLineValue "rgb:%opt{green}"
face global StatusCursor "rgb:%opt{bg0},rgb:%opt{fg2}"
face global Prompt "rgb:%opt{magenta}+b"
