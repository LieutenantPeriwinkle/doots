source "~/.config/kak/plugins/plug.kak/rc/plug.kak"
set-option global ui_options terminal_assistant=cat
set-option global startup_info_version 20211028
plug "andreyorst/plug.kak" noload config %{
    set-option global plug_always_ensure true
}
plug "kak-lsp/kak-lsp" do %{
    cargo install --locked --force --path .
}
plug "vktec/kaklip"
hook global WinSetOption filetype=(rust|c|cpp) %{
    lsp-enable-window
    lsp-auto-hover-enable
    set-option window lsp_hover_max_lines 12
    set-option window lsp_server_configuration rust.all_features=true
}

alias global lsd lsp-disable-window

define-command col80 %{
    add-highlighter buffer/ column 80 ColEnd
}

hook global WinSetOption filetype=(haskell) %{
    lsp-enable-window
    alias global lsd lsp-disable-window
}

map global user l %{: enter-user-mode lsp<ret>} -docstring "lsp mode"
colorscheme perilized

add-highlighter global/ number-lines -hlcursor
add-highlighter global/ show-whitespaces

define-command -hidden ctz %{
    terminal zsh
}
alias global > ctz

define-command restart %{
    lsp-disable
    lsp-disable-window
    lsp-enable
}

map global normal a ";li"
set-option global autoreload yes
# dvorak stuff
map global normal d h
map global normal h j
map global normal t k
map global normal n l
map global normal D H
map global normal H J
map global normal T K
map global normal N L

map global normal e d
map global normal <a-e> <a-d>

map global normal l t
map global insert <c-b> <c-n>
map global normal b n
map global normal X Jx

set-option global tabstop 4
set-option global indentwidth 4
hook global WinSetOption filetype=(c|cpp) %{
    # use tabs
    set-option global indentwidth 0
}

hook global WinSetOption filetype=(rust) %{
    map global insert <tab> '<a-;><a-gt>'
    hook window BufWritePre .* lsp-formatting-sync
    hook window -group rust-inlay-hints BufWritePost .* rust-analyzer-inlay-hints
    hook -once -always window WinSetOption filetype=.* %{
        remove-hooks window rust-inlay-hints
    }
}

