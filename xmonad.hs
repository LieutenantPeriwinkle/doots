{-# LANGUAGE LambdaCase #-}

import qualified Data.Map as M
import Data.Monoid
import System.Exit
import XMonad
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Layout.PerWorkspace
import XMonad.Layout.ResizableTile
import XMonad.Layout.Spacing
import qualified XMonad.StackSet as W
import XMonad.Util.Run

myBorderWidth = 0
myModMask = mod4Mask
myWorkspaces = ["w", "d", "m", "c", "g", "6", "7", "8", "9"]
myNormalBorderColor = "#dddddd"
myFocusedBorderColor = "#ff0000"
-- These keybinds likely only make sense on dvorak
myKeys conf@XConfig {XMonad.modMask = modm} =
  M.fromList $
    [ ((modm, xK_Return), spawn $ XMonad.terminal conf),
      ((modm, xK_e), spawn "rofi -show run"),
      ((modm .|. shiftMask, xK_quoteright), kill),
      ((modm, xK_space), sendMessage NextLayout),
      ((modm .|. shiftMask, xK_space), setLayout $ XMonad.layoutHook conf),
      ((modm, xK_n), refresh),
      ((modm, xK_p), windows W.focusDown),
      ((modm, xK_quoteright), windows W.focusUp),
      ((modm, xK_m), windows W.focusMaster),
      ((modm .|. shiftMask, xK_m), windows W.swapMaster),
      ((modm, xK_n), sendMessage Expand),
      ((modm, xK_d), sendMessage Shrink),
      ((modm, xK_h), sendMessage MirrorShrink),
      ((modm, xK_t), sendMessage MirrorExpand),
      ((modm, xK_y), withFocused $ windows . W.sink),
      ((modm, xK_w), sendMessage (IncMasterN 1)),
      ((modm, xK_v), sendMessage (IncMasterN (-1))),
      ((modm, xK_b), sendMessage ToggleStruts),
      ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess)),
      ((modm .|. shiftMask, xK_j), spawn "xmonad --recompile; xmonad --restart"),
      ((modm .|. shiftMask, xK_o), spawn "slock & sleep 0.3 && loginctl suspend"),
      ((modm, xK_Print), spawn "maim -s -u | xclip -selection clipboard -t image/png -i"),
      ((modm .|. shiftMask, xK_Print), spawn "maim -s -u ~/$(date +%s).png"),
      ((modm .|. shiftMask, xK_n), spawn "slock"),
      -- hack for my keyboard being dumb
      ((0, xK_F20), spawn "pactl set-sink-volume @DEFAULT_SINK@ +2%"),
      ((0, xK_F19), spawn "pactl set-sink-volume @DEFAULT_SINK@ -2%")
    ]
      ++
      [ ((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9],
          (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
      ]
      ++
      [ ((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_comma, xK_period] [0 ..],
          (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
      ]

myMouseBindings XConfig {XMonad.modMask = modm} =
  M.fromList
    [ ( (modm, button1),
        \w ->
          focus w >> mouseMoveWindow w
            >> windows W.shiftMaster
      ),
      ((modm, button2), \w -> focus w >> windows W.shiftMaster),
      ( (modm, button3),
        \w ->
          focus w >> mouseResizeWindow w
            >> windows W.shiftMaster
      )
    ]

myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full)
  where
    tiled = ResizableTall nmaster delta ratio slaves

    nmaster = 1

    ratio = 1 / 2

    delta = 3 / 100

    slaves = [1]

myManageHook =
  composeAll
    [ className =? "mist" --> doFloat,
      className =? "Dialog" --> doFloat,
      className =? "spyte" --> doFloat,
      resource =? "desktop_window" --> doIgnore,
      resource =? "kdesktop" --> doIgnore
    ]

myLogHook h = do
  updatePointer (0.5, 0.5) (0, 0)
  dynamicLogWithPP $
    xmobarPP
      { ppOutput = hPutStrLn h,
        ppVisible = xmobarColor "#2ca6e2" "" . wrap "<" ">",
        ppCurrent = xmobarColor "#3e991a" "" . wrap "{" "}",
        ppLayout = \case
          "ResizableTall" -> "||"
          "Mirror ResizableTall" -> "="
          "Full" -> "[]"
          x -> x,
        ppTitle = xmobarColor "#b99916" ""
      }

myStartupHook = return ()

main = do
  h <- spawnPipe "xmobar"
  xmonad (docks $ defaults h)

defaults h =
  ewmh
    def
      {
        terminal = "kitty",
        focusFollowsMouse = True,
        clickJustFocuses = False,
        borderWidth = myBorderWidth,
        modMask = myModMask,
        workspaces = myWorkspaces,
        normalBorderColor = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        keys = myKeys,
        mouseBindings = myMouseBindings,
        layoutHook = myLayout,
        manageHook = myManageHook,
        logHook = myLogHook h,
        startupHook = myStartupHook
      }
