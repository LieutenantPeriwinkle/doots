# heavily truncated
bindkey -e
autoload -Uz compinit
compinit

function precmd() {
    eval "$(starship init zsh)"
    print "\033]0;${PWD}\007"
}

if [ "$TERM" != "linux" ] && [ "$TERM" != "xterm" ]; then
    source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
    install_precmd
fi

alias ssh="TERM=xterm ssh"
alias ls='ls --color=auto'
alias furl="curl -fsSL"
alias :e="kak"
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=24'
alias ytm="youtube-dl --extract-audio --audio-format=mp3"
alias rr="git rev-parse --show-toplevel"

function cr {
    cd "$(git rev-parse --show-toplevel)"
}

source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=64'
ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=64'
ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=64'
ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=64'
ZSH_HIGHLIGHT_STYLES[command]='fg=28'
ZSH_HIGHLIGHT_STYLES[alias]='fg=28'
ZSH_HIGHLIGHT_STYLES[comment]='fg=238'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=28'
ZSH_HIGHLIGHT_STYLES[path]=''
export GPG_TTY=$(tty)

setopt AUTO_CD
